#ifndef STREAM9_MUTEX_HPP
#define STREAM9_MUTEX_HPP

#include <stream9/linux/error.hpp>
#include <stream9/outcome.hpp>
#include <stream9/time.hpp>

#include <pthread.h>

namespace stream9 {

class mutex
{
public:
    class attributes;

public:
    // essentials
    mutex();
    mutex(attributes const&);

    mutex(mutex const&) = delete;
    mutex& operator=(mutex const&) = delete;
    mutex(mutex&&) = delete;
    mutex& operator=(mutex&&) = delete;

    ~mutex() noexcept;

    // accessor
    auto* get(this auto&& self) noexcept { return &self.m_obj; }

    // query
    int priority_ceiling() const;

    // modifier
    void lock();
    void lock(system_time const& abstime);
    bool try_lock();

    outcome<void, lx::errc>
    unlock() noexcept;

    int set_priority_ceiling(int ceiling); // return old ceiling

private:
    ::pthread_mutex_t m_obj;
};

class mutex::attributes
{
public:
    // essentials
    attributes();

    attributes(attributes const&) = default;
    attributes& operator=(attributes const&) = default;
    attributes(attributes&&) = default;
    attributes& operator=(attributes&&) = default;

    ~attributes() noexcept;

    // accessor
    auto* get() const { return &m_obj; }

    // query
    int type() const;
    int pshared() const;
    int robust() const;

    // modifier
    void set_type(int type);
    void set_pshared(int pshared);
    void set_robust(int robustness);

private:
    ::pthread_mutexattr_t m_obj;
};

class unique_lock
{
public:
    unique_lock(mutex& m)
        : m_mutex { &m }
    {
        m_mutex->lock();
    }

    unique_lock(unique_lock const&) = delete;
    unique_lock& operator=(unique_lock const&) = delete;

    unique_lock(unique_lock&& o) noexcept
        : m_mutex { o.m_mutex }
    {
        o.m_mutex = nullptr;
    }

    unique_lock& operator=(unique_lock&& o) noexcept
    {
        if (m_mutex) {
            m_mutex->unlock();
        }
        m_mutex = o.m_mutex;
        o.m_mutex = nullptr;

        return *this;
    }

    ~unique_lock() noexcept
    {
        if (m_mutex) {
            m_mutex->unlock();
        }
    }

    void lock()
    {
        assert(m_mutex);
        m_mutex->lock();
    }

    void unlock() noexcept
    {
        assert(m_mutex);
        m_mutex->unlock();
    }

private:
    mutex* m_mutex; // non-null
};

} // namespace stream9

#endif // STREAM9_MUTEX_HPP
