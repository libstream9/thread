#ifndef STREAM9_CONDITION_VARIABLE_HPP
#define STREAM9_CONDITION_VARIABLE_HPP

#include <stream9/concepts.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/mutex.hpp>
#include <stream9/time.hpp>

#include <pthread.h>

namespace stream9 {

class condition_variable
{
public:
    class attributes;

public:
    // essentials
    condition_variable();
    condition_variable(attributes const&);

    condition_variable(condition_variable const&) = delete;
    condition_variable& operator=(condition_variable const&) = delete;
    condition_variable(condition_variable&&) = delete;
    condition_variable& operator=(condition_variable&&) = delete;

    ~condition_variable() noexcept;

    // commands
    void signal() noexcept;

    void broadcast() noexcept;

    template<predicate T>
    void wait(mutex&, T&&)
        noexcept(nothrow_predicate<T>);

    template<predicate T>
    void wait(mutex&, system_time const& abstime, T&&)
        noexcept(nothrow_predicate<T>);

private:
    ::pthread_cond_t m_id;
};

class condition_variable::attributes
{
public:
    // essentials
    attributes() noexcept;

    attributes(attributes const&) = default;
    attributes& operator=(attributes const&) = default;
    attributes(attributes&&) = default;
    attributes& operator=(attributes&&) = default;

    ~attributes() noexcept;

    // accessor
    auto* get(this auto&& self) { return &self.m_attr; }

    // query
    ::clockid_t clock() const;
    int pshared() const;

    // modifier
    void set_clock(::clockid_t clock_id);
    void set_pshared(int pshared);

private:
    ::pthread_condattr_t m_attr;
};

/*
 * implementation
 */
inline void condition_variable::
signal() noexcept
{
    ::pthread_cond_signal(&m_id); // never return error
}

inline void condition_variable::
broadcast() noexcept
{
    ::pthread_cond_broadcast(&m_id); // never return error
}

template<predicate T>
void condition_variable::
wait(mutex& m, T&& p)
    noexcept(nothrow_predicate<T>)
{
    try {
        while (std::forward<T>(p)() != true) {
            ::pthread_cond_wait(&m_id, m.get()); // never return error
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<predicate T>
void condition_variable::
wait(mutex& m, system_time const& abstime, T&& p)
    noexcept(nothrow_predicate<T>)
{
    try {
        while (std::forward<T>(p)() != true) {
            auto rc = ::pthread_cond_timedwait(&m_id, m.get(), &abstime.value());
            if (rc != 0) {
                throw error {
                    "pthread_cond_timedwait(3)",
                    lx::make_error_code(rc)
                };
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

inline condition_variable::attributes::
attributes() noexcept
{
    ::pthread_condattr_init(&m_attr);
}

inline condition_variable::attributes::
~attributes() noexcept
{
    ::pthread_condattr_destroy(&m_attr);
}

} // namespace stream9

#endif // STREAM9_CONDITION_VARIABLE_HPP
