#ifndef STREAM9_THREAD_POOL_HPP
#define STREAM9_THREAD_POOL_HPP

#include <functional>
#include <memory>

#include <stream9/namespace.hpp>

namespace stream9 {

class thread_pool
{
public:
    // essentials
    thread_pool();

    thread_pool(thread_pool const&) = delete;
    thread_pool& operator=(thread_pool const&) = delete;

    thread_pool(thread_pool&&) = default;
    thread_pool& operator=(thread_pool&&) = default;

    ~thread_pool() noexcept;

    // modifier
    void post(std::function<void()> task);

    // command
    void run();

private:
    struct impl;
    std::unique_ptr<impl> m_p;
};

} // namespace stream9

#endif // STREAM9_THREAD_POOL_HPP
