#ifndef STREAM9_THREAD_HPP
#define STREAM9_THREAD_HPP

#include "mutex.hpp"

#include <stream9/array_view.hpp>
#include <stream9/concepts.hpp>
#include <stream9/cstring_ptr.hpp>
#include <stream9/function.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/namespace.hpp>
#include <stream9/string.hpp>
#include <stream9/time.hpp>

#include <exception>

#include <pthread.h>

namespace stream9 {

class thread
{
public:
    class attributes;

public:
    // essentials
    template<typename F, typename... Args>
    thread(F&&, Args&&...)
        requires callable<F, Args...>;

    template<typename F, typename... Args>
    thread(class attributes const&, F&&, Args&&...)
        requires callable<F, Args...>;

    ~thread() noexcept;

    thread(thread const&) = delete;
    thread& operator=(thread const&) = delete;
    thread(thread&&) noexcept;
    thread& operator=(thread&&) noexcept;

    operator ::pthread_t () noexcept { return m_id; }

    // accessor
    auto& id() const { return m_id; }
    auto& id() { return m_id; }

    // query
    bool is_running() const;
    class attributes attributes();
    ::cpu_set_t affinity(size_t cpusetsize) const;
    ::clockid_t cpu_clock_id() const;
    string name() const;

    std::pair<int/*policy*/, struct ::sched_param>
    sched_param() const;

    // modifier
    void set_affinity(size_t cpusetsize, ::cpu_set_t const&);
    void set_name(cstring_ptr name); // up to 16 bytes including null terminator.
    void set_sched_param(int policy, struct ::sched_param const&);
    void set_sched_priolity(int priolity);


    // command
    void join();
    void join(system_time const&);
    bool try_join();

    void detach();

    // equality
    bool operator==(thread const&) const noexcept;

    static class attributes default_attributes();
    static void set_default_attributes(class attributes const&);

    static int concurrency() noexcept;
    static void set_concurrency(int new_level);

    static int set_cancel_state(int state); // returns old state
    static int set_cancel_type(int type); // returns old type

    [[noreturn]] static void exit(void* retval) noexcept;

private:
    static void* thread_main(void* arg);

private:
    struct context {
        mutex m_mutex;
        function<void()> m_func;
        std::exception_ptr m_exception = nullptr;
        bool m_detached : 1 = false;
        bool m_joined : 1 = false;
        bool m_running : 1 = false;
        bool m_destructed : 1 = false;

        template<typename F>
        context(F&& fn)
            : m_func { std::move(fn) }
        {}
    };

    pthread_t m_id;
    context* m_cxt; // non-null
};

class thread::attributes
{
public:
    // essentials
    attributes();
    attributes(thread&);
    attributes(::pthread_attr_t) noexcept;

    attributes(attributes const&) = default;
    attributes& operator=(attributes const&) = default;
    attributes(attributes&&) = default;
    attributes& operator=(attributes&&) = default;

    ~attributes() noexcept;

    // accessor
    auto* get(this auto&& self) { return &self.m_attr; }

    // query
    cpu_set_t affinity(size_t cpusetsize) const;
    int detach_state() const;
    size_t guard_size() const;
    int inherit_sched() const;
    struct ::sched_param sched_param() const;
    int sched_policy() const;
    int scope() const;
    ::sigset_t sigmask() const;
    array_view<char> stack() const;
    size_t stack_size() const;

    // modifier
    void set_affinity(size_t cpusetsize, cpu_set_t const&);
    void set_detach_state(int detachstate);
    void set_guard_size(size_t guardsize);
    void inherit_sched(int inheritsched);
    void set_sched_param(struct ::sched_param const&);
    void set_sched_policy(int policy);
    void set_scope(int scope);
    void set_sigmask(::sigset_t const& sigmask);
    void set_stack(array_view<char> stack);
    void set_stack_size(size_t stacksize);

private:
    ::pthread_attr_t m_attr;
};

/*
 * implementation
 */
template<typename F, typename... Args>
thread::
thread(F&& fn, Args&&... args)
    requires callable<F, Args...>
    try : m_cxt { new context { [fn, &args...]{ fn(std::forward<Args>(args)...); } } }
{
    auto rc = ::pthread_create(&m_id, nullptr, thread_main, m_cxt);
    if (rc != 0) {
        throw error {
            "pthread_create(3)",
            lx::make_error_code(rc),
        };
    }
}
catch (...) {
    rethrow_error();
}

template<typename F, typename... Args>
thread::
thread(class attributes const& attr, F&& fn, Args&&... args)
    requires callable<F, Args...>
    try : m_cxt { new context { [fn, &args...]{ fn(std::forward<Args>(args)...); } } }
{
    auto rc = ::pthread_create(&m_id, attr.get(), thread_main, m_cxt);
    if (rc != 0) {
        throw error {
            "pthread_create(3)",
            lx::make_error_code(rc),
        };
    }
}
catch (...) {
    rethrow_error();
}

inline class thread::attributes thread::
attributes()
{
    return *this;
}

inline int thread::
concurrency() noexcept
{
    return ::pthread_getconcurrency();
}

inline bool thread::
operator==(thread const& o) const noexcept
{
    return ::pthread_equal(m_id, o.m_id);
}

} // namespace stream9

#endif // STREAM9_THREAD_HPP
