#include <stream9/thread_pool.hpp>

#include <stream9/container.hpp>
#include <stream9/errors.hpp>
#include <stream9/nonmovable.hpp>
#include <stream9/node_array.hpp>

#include <condition_variable>
#include <deque>
#include <mutex>
#include <thread>
#include <vector>

#include <pthread.h>

namespace stream9 {

struct thread_pool::impl
{
    struct worker : private st9::nonmovable
    {
        impl* m_p; // non-null
        std::jthread m_thread;
        bool m_finished = false;

        worker(impl& p)
            try : m_p { &p }
            , m_thread { [&]() { run(); } }
        {
            ::pthread_setname_np(m_thread.native_handle(), "stream9::thread_pool::worker");
        }
        catch (...) {
            rethrow_error();
        }

        void run();
    };

    impl()
        : m_num_cores { std::max(std::thread::hardware_concurrency(), 1u) }
    {}

    void
    post(std::function<void()> work)
    {
        try {
            std::unique_lock lk { m_mutex };
            m_tasks.push_back(std::move(work));
            m_cv.notify_all();
        }
        catch (...) {
            rethrow_error();
        }
    }

    void
    run()
    {
        try {
            while (true) {
                std::unique_lock lk { m_mutex };

                clear_finished_workers();

                spawn_workers();

                if (m_workers.empty()) break;

                m_cv.wait(lk);
            }
        }
        catch (...) {
            rethrow_error();
        }
    }

    void spawn_workers();

    void
    clear_finished_workers() noexcept
    {
        m_workers.erase_if([](auto& w) {
            return w.m_finished;
        });
    }

    std::deque<std::function<void()>> m_tasks;
    st9::node_array<worker> m_workers;
    std::mutex m_mutex;
    std::condition_variable m_cv;
    unsigned int m_num_cores;
};

void thread_pool::impl::worker::
run()
{
    auto& tasks = m_p->m_tasks;
    auto& mutex = m_p->m_mutex;
    auto& cv = m_p->m_cv;

    try {
        while (true) {
            std::unique_lock lk { mutex };
            if (tasks.empty()) break;

            auto task = std::move(tasks.front());
            tasks.pop_front();
            lk.unlock();

            try {
                task();
            }
            catch (...) {
                print_error();
            }
        }

        std::unique_lock lk { mutex };
        m_finished = true;
        cv.notify_all();
    }
    catch (...) {
        std::unique_lock lk { mutex };
        m_finished = true;
        cv.notify_all();

        rethrow_error();
    }
}

void thread_pool::impl::
spawn_workers()
{
    try {
        for (std::size_t i = 0; i < m_tasks.size(); ++i) {
            if (m_workers.size() == m_num_cores) break;

            m_workers.emplace(*this);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

thread_pool::
thread_pool()
    try : m_p { std::make_unique<impl>() }
{}
catch (...) {
    rethrow_error();
}

thread_pool::~thread_pool() = default;

void thread_pool::
post(std::function<void()> work)
{
    try {
        m_p->post(std::move(work));
    }
    catch (...) {
        rethrow_error();
    }
}

void thread_pool::
run()
{
    try {
        m_p->run();
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9
