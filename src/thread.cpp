#include <stream9/thread.hpp>

#include <stream9/log.hpp>

namespace stream9 {

void* thread::
thread_main(void* arg)
{
    auto* cxt = reinterpret_cast<context*>(arg);
    unique_lock lk { cxt->m_mutex };

    cxt->m_running = true;

    try {
        lk.unlock();
        (cxt->m_func)();
        lk.lock();
    }
    catch (...) {
        lk.lock();
        cxt->m_exception = std::current_exception();
    }

    cxt->m_running = false;

    if (cxt->m_destructed) {
        lk.unlock();
        // no race here because thread is alredy destructed.
        delete cxt;
    }

    return nullptr;
}

/*
 * class thread
 */

thread::
thread(thread&& o) noexcept
    : m_id { o.m_id }
    , m_cxt { o.m_cxt }
{
    o.m_cxt = nullptr;
}

thread& thread::
operator=(thread&& o) noexcept
{
    using std::swap;
    swap(m_id, o.m_id);
    swap(m_cxt, o.m_cxt);
    return *this;
}

thread::
~thread() noexcept
{
    if (m_cxt == nullptr) return;

    try {
        join();
    }
    catch (...) {
        print_error(log::err());
    }

    try {
        unique_lock lk { m_cxt->m_mutex };

        if (!m_cxt->m_detached) {
            lk.unlock();
            // no race for mutex here, because thread is terminated.
            delete m_cxt;
        }
        else {
            m_cxt->m_destructed = true;
            lk.unlock();
        }
    }
    catch (...) { // lock fail
        delete m_cxt;

        print_error(log::err());
    }
}

bool thread::
is_running() const
{
    try {
        unique_lock lk { m_cxt->m_mutex };
        return m_cxt->m_running;
    }
    catch (...) {
        rethrow_error();
    }
}

::cpu_set_t thread::
affinity(size_t cpusetsize) const
{
    ::cpu_set_t cpuset;

    auto rc = ::pthread_getaffinity_np(m_id, cpusetsize, &cpuset);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }

    return cpuset;
}

::clockid_t thread::
cpu_clock_id() const
{
    ::clockid_t v;

    auto rc = ::pthread_getcpuclockid(m_id, &v);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }

    return v;
}

string thread::
name() const
{
    char buf[16];

    auto rc = ::pthread_getname_np(m_id, buf, sizeof(buf));
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }

    return buf;
}

std::pair<int/*policy*/, struct ::sched_param> thread::
sched_param() const
{
    std::pair<int, struct ::sched_param> rv;

    auto rc = ::pthread_getschedparam(m_id, &rv.first, &rv.second);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }

    return rv;
}

void thread::
set_affinity(size_t cpusetsize, ::cpu_set_t const& v)
{
    auto rc = ::pthread_setaffinity_np(m_id, cpusetsize, &v);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }
}

void thread::
set_name(cstring_ptr name)
{
    auto rc = ::pthread_setname_np(m_id, name);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }
}

void thread::
set_sched_param(int policy, struct ::sched_param const& v)
{
    auto rc = ::pthread_setschedparam(m_id, policy, &v);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }
}

void thread::
set_sched_priolity(int v)
{
    auto rc = ::pthread_setschedprio(m_id, v);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }
}

int thread::
set_cancel_state(int state)
{
    int old;

    auto rc = ::pthread_setcancelstate(state, &old);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }

    return old;
}

int thread::
set_cancel_type(int type)
{
    int old;

    auto rc = ::pthread_setcanceltype(type, &old);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }

    return old;
}

[[noreturn]] void thread::
exit(void* retval) noexcept
{
    ::pthread_exit(retval);
}

void thread::
set_concurrency(int new_level)
{
    auto rc = ::pthread_setconcurrency(new_level);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }
}

void thread::
join()
{
    try {
        unique_lock lk { m_cxt->m_mutex };

        if (!m_cxt->m_detached && !m_cxt->m_joined) {
            lk.unlock();
            auto rc = ::pthread_join(m_id, nullptr);
            if (rc != 0) {
                throw error {
                    "pthread_join(3)",
                    lx::make_error_code(rc),
                };
            }
            lk.lock();

            m_cxt->m_joined = true;

            if (m_cxt->m_exception) {
                auto e = m_cxt->m_exception;
                std::rethrow_exception(e);
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

void thread::
join(system_time const& t)
{
    try {
        unique_lock lk { m_cxt->m_mutex };

        if (!m_cxt->m_detached && !m_cxt->m_joined) {
            lk.unlock();
            auto rc = ::pthread_timedjoin_np(m_id, nullptr, &t.value());
            if (rc != 0) {
                throw error {
                    "pthread_timedjoin_np(3)",
                    lx::make_error_code(rc),
                };
            }
            lk.lock();

            m_cxt->m_joined = true;

            if (m_cxt->m_exception) {
                auto e = m_cxt->m_exception;
                std::rethrow_exception(e);
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

bool thread::
try_join()
{
    try {
        unique_lock lk { m_cxt->m_mutex };

        if (!m_cxt->m_detached && !m_cxt->m_joined) {
            lk.unlock();
            auto rc = ::pthread_tryjoin_np(m_id, nullptr);
            switch (rc) {
                case 0: break;
                case EBUSY: return false;
                default:
                    throw error {
                        "pthread_tryjoin_np(3)",
                        lx::make_error_code(rc),
                    };
            }
            lk.lock();

            m_cxt->m_joined = true;

            if (m_cxt->m_exception) {
                auto e = m_cxt->m_exception;
                std::rethrow_exception(e);
            }

            return true;
        }

        return false;
    }
    catch (...) {
        rethrow_error();
    }
}

void thread::
detach()
{
    try {
        unique_lock lk { m_cxt->m_mutex };

        if (!m_cxt->m_detached) {
            auto rc = ::pthread_detach(m_id);
            if (rc != 0) {
                throw error {
                    "pthread_detach(3)",
                    lx::make_error_code(rc),
                };
            }

            m_cxt->m_detached = true;
        }
    }
    catch (...) {
        rethrow_error();
    }
}

class thread::attributes thread::
default_attributes()
{
    ::pthread_attr_t attr;

    auto rc = ::pthread_getattr_default_np(&attr);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }

    return attr;
}

void thread::
set_default_attributes(class attributes const& v)
{
    auto rc = ::pthread_setattr_default_np(v.get());
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }
}

/*
 * class thread::attributes
 */
thread::attributes::
attributes()
{
    auto rc = ::pthread_attr_init(&m_attr);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }
}

thread::attributes::
attributes(thread& t)
{
    auto rc = ::pthread_getattr_np(t, &m_attr);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }
}

thread::attributes::
attributes(::pthread_attr_t v) noexcept
    : m_attr { std::move(v) }
{}

thread::attributes::
~attributes() noexcept
{
    try {
        auto rc = ::pthread_attr_destroy(&m_attr);
        if (rc != 0) {
            throw error {
                lx::make_error_code(rc)
            };
        }
    }
    catch (...) {
        print_error(log::err());
    }
}

cpu_set_t thread::attributes::
affinity(size_t cpusetsize) const
{
    cpu_set_t set;

    auto rc = ::pthread_attr_getaffinity_np(&m_attr, cpusetsize, &set);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }

    return set;
}

int thread::attributes::
detach_state() const
{
    int detachstate;

    auto rc = ::pthread_attr_getdetachstate(&m_attr, &detachstate);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }

    return detachstate;
}

size_t thread::attributes::
guard_size() const
{
    size_t guardsize;

    auto rc = ::pthread_attr_getguardsize(&m_attr, &guardsize);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }

    return guardsize;
}

int thread::attributes::
inherit_sched() const
{
    int v;

    auto rc = ::pthread_attr_getinheritsched(&m_attr, &v);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }

    return v;
}

struct ::sched_param thread::attributes::
sched_param() const
{
    struct ::sched_param v;

    auto rc = ::pthread_attr_getschedparam(&m_attr, &v);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }

    return v;
}

int thread::attributes::
sched_policy() const
{
    int v;

    auto rc = ::pthread_attr_getschedpolicy(&m_attr, &v);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }

    return v;
}

int thread::attributes::
scope() const
{
    int v;

    auto rc = ::pthread_attr_getscope(&m_attr, &v);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }

    return v;
}

::sigset_t thread::attributes::
sigmask() const
{
    ::sigset_t v;

    auto rc = ::pthread_attr_getsigmask_np(&m_attr, &v);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }

    return v;
}

array_view<char> thread::attributes::
stack() const
{
    char* addr = nullptr;
    size_t size = 0;

    auto rc = ::pthread_attr_getstack(&m_attr, reinterpret_cast<void**>(&addr), &size);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }

    return { addr, size };
}

size_t thread::attributes::
stack_size() const
{
    size_t size;

    auto rc = ::pthread_attr_getstacksize(&m_attr, &size);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }

    return size;
}

void thread::attributes::
set_affinity(size_t cpusetsize, cpu_set_t const& cpuset)
{
    auto rc = ::pthread_attr_setaffinity_np(&m_attr, cpusetsize, &cpuset);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }
}

void thread::attributes::
set_detach_state(int detachstate)
{
    auto rc = ::pthread_attr_setdetachstate(&m_attr, detachstate);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }
}

void thread::attributes::
set_guard_size(size_t guardsize)
{
    auto rc = ::pthread_attr_setguardsize(&m_attr, guardsize);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }
}

void thread::attributes::
inherit_sched(int inheritsched)
{
    auto rc = ::pthread_attr_setinheritsched(&m_attr, inheritsched);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }
}

void thread::attributes::
set_sched_param(struct ::sched_param const& v)
{
    auto rc = ::pthread_attr_setschedparam(&m_attr, &v);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }
}

void thread::attributes::
set_sched_policy(int policy)
{
    auto rc = ::pthread_attr_setschedpolicy(&m_attr, policy);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }
}

void thread::attributes::
set_scope(int scope)
{
    auto rc = ::pthread_attr_setscope(&m_attr, scope);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }
}

void thread::attributes::
set_sigmask(::sigset_t const& sigmask)
{
    auto rc = ::pthread_attr_setsigmask_np(&m_attr, &sigmask);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }
}

void thread::attributes::
set_stack(array_view<char> stack)
{
    auto rc = ::pthread_attr_setstack(&m_attr, stack.data(), stack.size());
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }
}

void thread::attributes::
set_stack_size(size_t v)
{
    auto rc = ::pthread_attr_setstacksize(&m_attr, v);
    if (rc != 0) {
        throw error {
            lx::make_error_code(rc)
        };
    }
}

} // namespace stream9
