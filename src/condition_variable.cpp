#include <stream9/condition_variable.hpp>

#include <stream9/log.hpp>

#include <pthread.h>

namespace stream9 {

/*
 * class condition_variable
 */
condition_variable::
condition_variable()
{
    auto rc = ::pthread_cond_init(&m_id, nullptr);
    if (rc != 0) {
        throw error {
            "pthread_cond_init(3)",
            lx::make_error_code(rc)
        };
    }
}

condition_variable::
condition_variable(attributes const& a)
{
    auto rc = ::pthread_cond_init(&m_id, a.get());
    if (rc != 0) {
        throw error {
            "pthread_cond_init(3)",
            lx::make_error_code(rc)
        };
    }
}

condition_variable::
~condition_variable() noexcept
{
    try {
        auto rc = ::pthread_cond_destroy(&m_id);
        if (rc != 0) {
            throw error {
                "pthread_cond_destroy(3)",
                lx::make_error_code(rc)
            };
        }
    }
    catch (...) {
        print_error(log::err());
    }
}

/*
 * class condition_variable::attributes
 */
::clockid_t condition_variable::attributes::
clock() const
{
    ::clockid_t v;

    auto rc = ::pthread_condattr_getclock(&m_attr, &v);
    if (rc != 0) {
        throw error {
            "pthread_condattr_getclock(3)",
            lx::make_error_code(rc)
        };
    }

    return v;
}

int condition_variable::attributes::
pshared() const
{
    int v;

    auto rc = ::pthread_condattr_getpshared(&m_attr, &v);
    if (rc != 0) {
        throw error {
            "pthread_condattr_getpshared(3)",
            lx::make_error_code(rc)
        };
    }

    return v;
}

void condition_variable::attributes::
set_clock(::clockid_t clock_id)
{
    auto rc = ::pthread_condattr_setclock(&m_attr, clock_id);
    if (rc != 0) {
        throw error {
            "pthread_condattr_setclock(3)",
            lx::make_error_code(rc)
        };
    }
}

void condition_variable::attributes::
set_pshared(int pshared)
{
    auto rc = ::pthread_condattr_setpshared(&m_attr, pshared);
    if (rc != 0) {
        throw error {
            "pthread_condattr_setpshared(3)",
            lx::make_error_code(rc)
        };
    }
}

} // namespace stream9
