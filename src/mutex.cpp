#include <stream9/mutex.hpp>

#include <stream9/linux/error.hpp>
#include <stream9/log.hpp>

namespace stream9 {

mutex::
mutex()
{
    try {
        attributes a;
        a.set_type(PTHREAD_MUTEX_ERRORCHECK);
        a.set_robust(PTHREAD_MUTEX_ROBUST);

        if (auto rc = ::pthread_mutex_init(&m_obj, a.get()); rc != 0) {
            throw error {
                "pthread_mutex_init(3)",
                lx::make_error_code(rc)
            };
        }
    }
    catch (...) {
        rethrow_error();
    }
}

mutex::
mutex(attributes const& a)
{
    if (auto rc = ::pthread_mutex_init(&m_obj, a.get()); rc != 0) {
        throw error {
            "pthread_mutex_init(3)",
            lx::make_error_code(rc)
        };
    }
}

mutex::
~mutex() noexcept
{
    try {
        auto rc = ::pthread_mutex_destroy(&m_obj);
        if (rc != 0) {
            throw error {
                "pthread_mutex_destroy(3)",
                lx::make_error_code(rc)
            };
        }
    }
    catch (...) {
        print_error(log::err());
    }
}

int mutex::
priority_ceiling() const
{
    try {
        int v;

        auto rc = ::pthread_mutex_getprioceiling(&m_obj, &v);
        if (rc != 0) {
            throw error {
                "pthread_mutex_getprioceiling(3)",
                lx::make_error_code(rc)
            };
        }

        return v;
    }
    catch (...) {
        rethrow_error();
    }
}

void mutex::
lock()
{
    auto rc = ::pthread_mutex_lock(&m_obj);
    switch (rc) {
        case 0:
            break;
        case EOWNERDEAD:
            rc = ::pthread_mutex_consistent(&m_obj);
            if (rc != 0) {
                throw error {
                    "pthread_mutex_consistent(3)",
                    lx::make_error_code(rc)
                };
            }
            break;
        case EAGAIN:
        case EDEADLK:
        case EINVAL:
        case ENOTRECOVERABLE:
        default:
            throw error {
                "pthread_mutex_lock(3)",
                lx::make_error_code(rc)
            };
    }
}

void mutex::
lock(system_time const& abstime)
{
    auto rc = ::pthread_mutex_timedlock(&m_obj, &abstime.value());
    switch (rc) {
        case 0:
            break;
        case EOWNERDEAD:
            rc = ::pthread_mutex_consistent(&m_obj);
            if (rc != 0) {
                throw error {
                    "pthread_mutex_consistent(3)",
                    lx::make_error_code(rc)
                };
            }
            break;
        case ETIMEDOUT:
        case EAGAIN:
        case EDEADLK:
        case EINVAL:
        case ENOTRECOVERABLE:
        default:
            throw error {
                "pthread_mutex_timedlock(3)",
                lx::make_error_code(rc)
            };
    }
}

bool mutex::
try_lock()
{
    auto rc = ::pthread_mutex_trylock(&m_obj);
    switch (rc) {
        case 0:
            return true;
        case EBUSY:
            return false;
        case EOWNERDEAD:
            rc = ::pthread_mutex_consistent(&m_obj);
            if (rc != 0) {
                throw error {
                    "pthread_mutex_consistent(3)",
                    lx::make_error_code(rc)
                };
            }
            break;
        case EAGAIN:
        case EINVAL:
        case ENOTRECOVERABLE:
        default:
            throw error {
                "pthread_mutex_trylock(3)",
                lx::make_error_code(rc)
            };
    }

    return static_cast<lx::errc>(rc);
}

outcome<void, lx::errc> mutex::
unlock() noexcept
{
    return static_cast<lx::errc>(::pthread_mutex_unlock(&m_obj));
}

int mutex::
set_priority_ceiling(int v)
{
    try {
        int old;

        auto rc = ::pthread_mutex_setprioceiling(&m_obj, v, &old);
        if (rc != 0) {
            throw error {
                "pthread_mutex_setprioceiling(3)",
                lx::make_error_code(rc)
            };
        }

        return old;
    }
    catch (...) {
        rethrow_error();
    }
}

/*
 * class mutex::attributes
 */
mutex::attributes::
attributes()
{
    if (auto rc = ::pthread_mutexattr_init(&m_obj); rc != 0) {
        throw error {
            "pthread_mutexattr_init(3)",
            lx::make_error_code(rc)
        };
    }
}

mutex::attributes::
~attributes() noexcept
{
    try {
        if (auto rc = ::pthread_mutexattr_destroy(&m_obj); rc != 0) {
            throw error {
                "pthread_mutexattr_destroy(3)",
                lx::make_error_code(rc)
            };
        }
    }
    catch (...) {
        print_error(log::err());
    }
}

int mutex::attributes::
type() const
{
    int v;

    if (auto rc = ::pthread_mutexattr_gettype(&m_obj, &v); rc != 0) {
        throw error {
            "pthread_mutexattr_gettype(3)",
            lx::make_error_code(rc)
        };
    }

    return v;
}

int mutex::attributes::
pshared() const
{
    int v;

    if (auto rc = ::pthread_mutexattr_getpshared(&m_obj, &v); rc != 0) {
        throw error {
            "pthread_mutexattr_getpshared(3)",
            lx::make_error_code(rc)
        };
    }

    return v;
}

int mutex::attributes::
robust() const
{
    int v;

    if (auto rc = ::pthread_mutexattr_getrobust(&m_obj, &v); rc != 0) {
        throw error {
            "pthread_mutexattr_getrobust(3)",
            lx::make_error_code(rc)
        };
    }

    return v;
}

void mutex::attributes::
set_type(int type)
{
    if (auto rc = ::pthread_mutexattr_settype(&m_obj, type); rc != 0) {
        throw error {
            "pthread_mutexattr_settype(3)",
            lx::make_error_code(rc)
        };
    }
}

void mutex::attributes::
set_pshared(int pshared)
{
    if (auto rc = ::pthread_mutexattr_setpshared(&m_obj, pshared); rc != 0) {
        throw error {
            "pthread_mutexattr_setpshared(3)",
            lx::make_error_code(rc)
        };
    }
}

void mutex::attributes::
set_robust(int robustness)
{
    if (auto rc = ::pthread_mutexattr_setrobust(&m_obj, robustness); rc != 0) {
        throw error {
            "pthread_mutexattr_setrobust(3)",
            lx::make_error_code(rc)
        };
    }
}

} // namespace stream9
